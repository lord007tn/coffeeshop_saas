import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getShopCategories } from "../../../../actions/category.actions";
import Spinner from "../../../utils/Spinner";
import { getShopItemsByCategory } from "../../../../actions/item.actions";
const Categories = ({ category, getShopCategories, getShopItemsByCategory, shopID }) => {
  const [selectedCategory, setSelectedCategory] = useState({});

  const selectCategory = (e, category) => {
    e.preventDefault()
    getShopItemsByCategory(category._id, shopID)
    setSelectedCategory(category);
  }
  useEffect(() => {
    getShopCategories(shopID);
  }, [category.loading]);
  return category.loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <button className="my-auto appearance-none focus:outline-none">
        <svg
          width="30"
          height="30"
          viewBox="0 0 30 30"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M30 15C30 6.7125 23.2875 -2.93413e-07 15 -6.55671e-07C6.7125 -1.01793e-06 -2.20076e-06 6.7125 -2.56302e-06 15C-2.92528e-06 23.2875 6.7125 30 15 30C23.2875 30 30 23.2875 30 15ZM12 15L18 9L18 21L12 15Z"
            fill="#303135"
          />
        </svg>
      </button>
      <div className="flex flex-none justify-between w-11/12 overflow-hidden">
        {category.categories &&
          category.categories.map((category) => {
            return (
              <div
                onClick={(e) => {
                  selectCategory(e, category)
                }}
                key={category._id}
                className={`hover:bg-primary hover:text-main text-primary font-Montserrat font-medium text-lg rounded-lg p-2 my-auto mr-2 cursor-pointer ${
                  selectedCategory._id === category._id
                    ? "bg-primary text-main "
                    : "text-primary"
                }`}
              >
                <span>{category.categoryName}</span>
              </div>
            );
          })}
      </div>

      <button className="my-auto appearance-none focus:outline-none">
        <svg
          width="30"
          height="30"
          viewBox="0 0 30 30"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M-6.55671e-07 15C-2.93413e-07 23.2875 6.7125 30 15 30C23.2875 30 30 23.2875 30 15C30 6.7125 23.2875 -2.92528e-06 15 -2.56302e-06C6.7125 -2.20076e-06 -1.01793e-06 6.7125 -6.55671e-07 15ZM18 15L12 21L12 9L18 15Z"
            fill="#303135"
          />
        </svg>
      </button>
    </Fragment>
  );
};

Categories.propTypes = {
  category: PropTypes.object.isRequired,
  shopID: PropTypes.string.isRequired,
  getShopCategories: PropTypes.func.isRequired,
  getShopItemsByCategory: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  category: state.category,
});

const mapDispatchToProps = {
  getShopCategories,
  getShopItemsByCategory
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
