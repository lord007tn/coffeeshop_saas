import React from "react";
import PropTypes from "prop-types";
import PendingOrdersItems from "./PendingOrdersItems";

const PendingOrders = (props) => {
  return (
    <div className="bg-transparent flex flex-none flex-col h-2/5 my-4">
      <h1 className="font-Montserrat text-primary font-extrabold text-2xl">
        Pending Orders
      </h1>
      <div className=" my-4 border-b-2 border-main overflow-y-auto">
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
        <PendingOrdersItems />
      </div>
    </div>
  );
};

PendingOrders.propTypes = {};

export default PendingOrders;
