import React from "react";
import PropTypes from "prop-types";

const PendingOrdersItems = (props) => {
  return (
    <div className="flex flex-1 justify-start w-full my-4">
      <div
        className="h-12 w-12 bg-cover rounded-lg shadow"
        style={{
          backgroundImage:
            "url()",
        }}
      />
      <div className=" mx-3 w-4/6">
        <h1 className="font-Montserrat text-primary font-bold">Table 12</h1>
        <p className="font-Montserrat text-primary text-base w-full truncate">
          kqjhdsgksqjss sdfg dsfg fdsg sdf gsdf gd fsgs
        </p>
      </div>
      <p className="font-Montserrat text-shadow text-sm">3m ago</p>
    </div>
  );
};

PendingOrdersItems.propTypes = {};

export default PendingOrdersItems;
