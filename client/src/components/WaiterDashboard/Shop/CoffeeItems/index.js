import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const CoffeeItems = ({ item }) => {
  return (
    <Fragment>
      <div className="grid grid-cols-3 xl:gap-8 lg:gap-4 gap-2">
        {item.items &&
          item.items.map((item) => {
            return (
              <button
                type="button"
                key={item._id}
                className="flex flex-1 justify-between rounded-lg border-2 border-gray-200 p-2 focus:bg-secondary focus:outline-none"
              >
                <div className="flex flex-col w-1/2 mx-auto">
                  <div className="font-Montserrat font-medium xl:text-lg text-base text-center text-primary truncate my-4">
                    {item.itemName}
                  </div>
                  <div className="font-Montserrat font-bold xl:text-2xl text-lg text-center text-primary">
                    {item.price} DT
                  </div>
                </div>
                <div
                  className=" w-1/2 h-24 bg-cover rounded-lg shadow"
                  style={{
                    backgroundImage: "url()",
                  }}
                />
              </button>
            );
          })}
      </div>
    </Fragment>
  );
};

CoffeeItems.propTypes = {
  item: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  item: state.item,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeItems);
