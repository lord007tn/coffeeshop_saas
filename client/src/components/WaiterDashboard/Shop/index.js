import React, { useEffect } from "react";
import PropTypes from "prop-types";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Navbar from "./Navbar";
import Categories from "./Categories";
import CoffeeItems from "./CoffeeItems";
import CurrentOrder from "./CurrentOrder";
import PendingOrders from "./PendingOrders";
import { getShop } from "../../../actions/shop.actions";
import Spinner from "../../utils/Spinner";
const Shop = ({ match, auth, shop, getShop }) => {
  useEffect(() => {
    getShop(match.params.shopID);
  }, [auth.loading]);

  return shop.loading ? (
    <Spinner />
  ) : (
    <div className=" h-screen flex flex-1 bg-whity overflow-hidden">
      <div className="flex bg-transparent w-4/6 overflow-hidden flex-col py-6 px-5">
        <Navbar
          shopName={shop.shop.shopName}
          waiterFirstName={auth.user.firstName}
          waiterLastName={auth.user.lastName}
        />
        <div className="bg-transparent overflow-x-hidden whitespace-nowrap h-32 content-center py-2 flex flex-row justify-between">
          <Categories shopID={shop.shop._id} />
        </div>
        <div className="bg-transparent overflow-y-auto px-6 mt-4 mb-4 border-b-2 border-main h-screen">
          <CoffeeItems />
        </div>
      </div>
      <div className="bg-secondary flex flex-1 w-2/6 flex-col px-8 pb-10 pt-6 border-l-2 border-main shadow-xl">
        <CurrentOrder />
        <PendingOrders />
      </div>
    </div>
  );
};

Shop.propTypes = {
  match: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  shop: PropTypes.object.isRequired,
  getShop: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  shop: state.shop,
});

const mapDispatchToProps = {
  getShop,
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop);
