import React from "react";
import PropTypes from "prop-types";

const Navbar = ({shopName, waiterFirstName, waiterLastName}) => {
  return (
    <div className="bg-transparent flex justify-between">
      <div className="">
        <h1 className="font-Montserrat font-bold text-4xl capitalize text-primary">{ shopName }</h1>
        <h4 className="font-Montserrat font-medium text-lg capitalize text-primary">{waiterFirstName} { waiterLastName }</h4>
      </div>
      <div className="rounded-full h-16 w-16 flex items-center justify-center">
        Circle
      </div>
    </div>
  );
};

Navbar.propTypes = {
  shopName: PropTypes.string.isRequired,
  waiterFirstName: PropTypes.string.isRequired,
  waiterLastName: PropTypes.string.isRequired,
};

export default Navbar;
