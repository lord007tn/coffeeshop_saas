import React from "react";
import PropTypes from "prop-types";
import CurrentOrderItem from "./CurrentOrderItem";
import { connect } from 'react-redux'

const CurrentOrder = (props) => {
  return (
    <div className="bg-transparent h-3/5 flex flex-col ">
      <div className="flex flex-none justify-between">
        <h1 className="font-Montserrat text-primary font-extrabold text-2xl">
          Current Order
        </h1>
        <button
          type="button"
          className="appearance-none focus:outline-none bg-primary text-main text-base font-semibold font-Montserrat py-1 px-2 rounded-lg"
        >
          Clear all
        </button>
      </div>
      <div className=" my-4 border-b-2 border-main overflow-y-auto">
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
        <CurrentOrderItem />
      </div>
      <div>
        <button
          type="button"
          className="flex justify-between appearance-none focus:outline-none w-full bg-primary text-main text-lg font-semibold font-Montserrat p-3 my-2 rounded-lg"
        >
          Place order
          <p> 36.5 DT</p>
        </button>
      </div>
    </div>
  );
};
export default CurrentOrder;
