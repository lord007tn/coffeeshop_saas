import React from 'react'
import PropTypes from 'prop-types'

const CurrentOrderItem = props => {
    return (
<div className="flex flex-none justify-between w-full my-4">
        <div
          className=" h-12 bg-cover rounded-lg shadow w-2/12"
          style={{
            backgroundImage:
              "url()",
          }}
        />
        <p className="font-Montserrat text-primary text-base mx-3 my-auto w-4/12">
          Cappucin qsdg dsfqsdfqdsfqsdfq
        </p>
        <div className="flex justify-center w-4/12">
          <button className=" appearance-none focus:outline-none mx-1">
            <svg
              width="25"
              height="25"
              viewBox="0 0 25 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="12.5" cy="12.5" r="12.5" fill="#BDBDBD" />
              <path
                d="M7.32233 14.2665V11.4888H18.4334V14.2665H7.32233Z"
                fill="#303135"
              />
            </svg>
          </button>
          <p className="font-Montserrat text-primary font-bold text-lg my-auto mx-2">
            1
          </p>
          <button className=" appearance-none focus:outline-none mx-1">
            <svg
              width="25"
              height="25"
              viewBox="0 0 25 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="12.5" cy="12.5" r="12.5" fill="#BDBDBD" />
              <path
                d="M7.32233 14.2665V11.4888H18.4334V14.2665H7.32233Z"
                fill="#303135"
              />
              <path
                d="M11.489 7.32202L14.2668 7.32202L14.2668 18.4331L11.489 18.4331L11.489 7.32202Z"
                fill="#303135"
              />
            </svg>
          </button>
        </div>
        <p className=" font-Montserrat text-primary font-bold text-xl my-auto w-3/12 whitespace-nowrap">
          12.4 DT
        </p>
      </div>
    )
}

CurrentOrderItem.propTypes = {

}

export default CurrentOrderItem
