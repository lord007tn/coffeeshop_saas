import React, { useState, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import { login } from "../../../actions/auth.actions";
import Spinner from "../../utils/Spinner";
const Login = ({ auth, login }) => {
  const [FormData, setFormData] = useState({
    loginInfo: "",
    password: "",
  });
  const { loginInfo, password } = FormData;
  const onChange = (e) =>
    setFormData({ ...FormData, [e.target.name]: e.target.value });
  const onSubmit = (e) => {
    e.preventDefault();
    login(loginInfo, password);
    setFormData({ loginInfo: "", password: "" });
  };
  if (auth.isAuthenticated) {
    return <Redirect to={`/${auth.user.shop}/main`} />;
  }
  return auth.loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <div className="min-h-screen  flex items-center justify-center bg-secondary">
        <div className=" max-w-full w-1/2">
          <div className=" bg-primary rounded-lg shadow-xl px-12 py-16">
            <div className="center">
              <h1 className="text-main text-center text-5xl font-bold font-Montserrat">
                Welcome Back
              </h1>
              <h3 className="text-main text-center font-light text-2xl font-Montserrat">
                We&quot;re glad to see you again !
              </h3>
            </div>
            <form className="mt-8" onSubmit={(e) => onSubmit(e)}>
              <div className="rounded-lg shadow-sm">
                <div className="mb-6">
                  <input
                    id="loginInfo"
                    name="loginInfo"
                    type="text"
                    placeholder="Email or Phone Number"
                    value={loginInfo}
                    onChange={(e) => onChange(e)}
                    required
                    className=" font-Montserrat appearance-none rounded-lg relative block w-full px-3 py-2 border border-gray-100 text-primary focus:outline-none focus:ring-2 focus:ring-blue-400"
                  />
                </div>
                <div className="-mt-px">
                  <input
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => onChange(e)}
                    required
                    className="font-Montserrat appearance-none rounded-lg relative block w-full px-3 py-2 border border-gray-100 text-primary focus:outline-none focus:ring-2 focus:ring-blue-400"
                  />
                </div>
                <div className="text-sm leading-5 my-1">
                  <Link
                    to="#"
                    className="font-Montserrat font-semibold text-base  text-main hover:text-white focus:outline-none focus:underline transition ease-in-out duration-150"
                  >
                    Forgot your password?
                  </Link>
                </div>
              </div>
              <div className="mt-6">
                <button
                  type="submit"
                  className=" font-Montserrat font-bold relative w-full justify-center py-2 px-4 border border-transparent rounded-lg text-primary bg-whity hover:bg-white focus:outline-none focus:bg-white transition duration-150 ease-in-out"
                >
                  Log In
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { login })(Login);
