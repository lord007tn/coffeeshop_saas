import api from "../utils/api";
import {
  GET_SHOP_ITEMS,
  CREATE_SHOP_ITEM,
  DELETE_SHOP_ITEM,
  GET_SHOP_ITEM,
  SHOP_ITEM_ERROR,
  UPDATE_SHOP_ITEM,
  GET_SHOP_ITEMS_BY_CATEGORY
} from "./types";
export const getShopItems = (shopID) => async (dispatch) => {
  try {
    const res = await api.get(`/api/items/${shopID}`);
    dispatch({
      type: GET_SHOP_ITEMS,
      payload: res.data.items,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
export const getShopItemsByCategory = (categoryID, shopID) => async (dispatch) => {
  try {
    const res = await api.get(`/api/items/${shopID}/${categoryID}/items`);
    dispatch({
      type: GET_SHOP_ITEMS_BY_CATEGORY,
      payload: res.data.items,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
export const getShopItem = (itemID, shopID) => async (dispatch) => {
  try {
    const res = await api.get(`/api/items/${shopID}/${itemID}`);
    dispatch({
      type: GET_SHOP_ITEM,
      payload: res.data.item,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
export const createShopItem = (itemData, shopID) => async (
  dispatch
) => {
  try {
    const res = await api.post(
      `/api/items/${shopID}/create`,
      itemData
    );
    dispatch({
      type: CREATE_SHOP_ITEM,
      payload: res.data.savedItem,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
export const updateShopItem = (itemData, itemID, shopID) => async (
  dispatch
) => {
  try {
    const res = await api.put(
      `/api/items/${shopID}/${itemID}/update`,
      itemData
    );
    dispatch({
      type: UPDATE_SHOP_ITEM,
      payload: res.data.updatedItem,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
export const deleteShopItem = (itemID, shopID) => async (dispatch) => {
  try {
    const res = await api.delete(
      `/api/items/${shopID}/${itemID}/delete`
    );
    dispatch({
      type: DELETE_SHOP_ITEM,
      payload: res.data.deletedItem,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ITEM_ERROR,
      payload: err,
    });
  }
};
