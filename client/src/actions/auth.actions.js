import api from "../utils/api";
import {
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./types";

import setAuthToken from "../utils/setAuthToken";

//Load user

export const loadUser = () => async (dispatch) => {
  dispatch({
    type: USER_LOADING,
  });
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }

  try {
    const res = await api.get("/api/authcheck");
    console.log(res);
    dispatch({
      type: USER_LOADED,
      payload: res.data,
    });
  } catch (err) {
    console.log(err.response);
    dispatch({
      type: AUTH_ERROR,
      payload: err,
    });
  }
};

//Register

export const register = ({
  firstName,
  lastName,
  email,
  phoneNumber,
  password,
}) => async (dispatch) => {
  dispatch({
    type: USER_LOADING,
  });
  const body = JSON.stringify({
    firstName,
    lastName,
    email,
    phoneNumber,
    password,
  });

  try {
    await api.post("/api/register", body);
    dispatch({
      type: REGISTER_SUCCESS,
    });
  } catch (err) {
    dispatch({
      type: REGISTER_FAIL,
      payload: err,
    });
  }
};

//Login

export const login = (loginInfo, password) => async (dispatch) => {
  dispatch({
    type: USER_LOADING,
  });

  try {
    const res = await api.post("/api/login", {
      loginInfo: loginInfo,
      password: password,
    });
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    });
    dispatch(loadUser());
  } catch (err) {
    dispatch({
      type: LOGIN_FAIL,
      payload: err,
    });
  }
};

//Logout

export const logout = () => async (dispatch) => {
  dispatch({
    type: USER_LOADING,
  });
  dispatch({
    type: LOGOUT,
  });
};
