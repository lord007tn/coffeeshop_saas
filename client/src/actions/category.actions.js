import api from "../utils/api";
import {
  GET_SHOP_CATEGORIES,
  CREATE_SHOP_CATEGORY,
  DELETE_SHOP_CATEGORY,
  GET_SHOP_CATEGORY,
  SHOP_CATEGORY_ERROR,
  UPDATE_SHOP_CATEGORY,
} from "./types";
export const getShopCategories = (shopID) => async (dispatch) => {
  try {
    const res = await api.get(`/api/categories/${shopID}`);
    dispatch({
      type: GET_SHOP_CATEGORIES,
      payload: res.data.categories,
    });
  } catch (err) {
    dispatch({
      type: SHOP_CATEGORY_ERROR,
      payload: err,
    });
  }
};

export const getShopCategory = (categoryID, shopID) => async (dispatch) => {
  try {
    const res = await api.get(`/api/categories/${shopID}/${categoryID}`);
    dispatch({
      type: GET_SHOP_CATEGORY,
      payload: res.data.category,
    });
  } catch (err) {
    dispatch({
      type: SHOP_CATEGORY_ERROR,
      payload: err,
    });
  }
};
export const createShopCategory = (categoryData, shopID) => async (
  dispatch
) => {
  try {
    const res = await api.post(
      `/api/categories/${shopID}/create`,
      categoryData
    );
    dispatch({
      type: CREATE_SHOP_CATEGORY,
      payload: res.data.savedCategory,
    });
  } catch (err) {
    dispatch({
      type: SHOP_CATEGORY_ERROR,
      payload: err,
    });
  }
};
export const updateShopCategory = (categoryData, categoryID, shopID) => async (
  dispatch
) => {
  try {
    const res = await api.put(
      `/api/categories/${shopID}/${categoryID}/update`,
      categoryData
    );
    dispatch({
      type: UPDATE_SHOP_CATEGORY,
      payload: res.data.updatedCategory,
    });
  } catch (err) {
    dispatch({
      type: SHOP_CATEGORY_ERROR,
      payload: err,
    });
  }
};
export const deleteShopCategory = (categoryID, shopID) => async (dispatch) => {
  try {
    const res = await api.delete(
      `/api/categories/${shopID}/${categoryID}/delete`
    );
    dispatch({
      type: DELETE_SHOP_CATEGORY,
      payload: res.data.deletedCategory,
    });
  } catch (err) {
    dispatch({
      type: SHOP_CATEGORY_ERROR,
      payload: err,
    });
  }
};
