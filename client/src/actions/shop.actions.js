import api from "../utils/api";
import { GET_SHOP, SHOP_ERROR } from "./types";
export const getShop = (id) => async (dispatch) => {
  try {
    const res = await api.get(`/api/shops/${id}`);
    dispatch({
      type: GET_SHOP,
      payload: res.data.shop,
    });
  } catch (err) {
    dispatch({
      type: SHOP_ERROR,
      payload: err,
    });
  }
};
