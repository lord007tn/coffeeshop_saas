import React, { Fragment, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
//Redux
import { Provider } from "react-redux";
import store from "./store";
//checkAuthMiddleware
import setAuthToken from "./utils/setAuthToken";
import { loadUser } from "./actions/auth.actions";
import { LOGOUT } from "./actions/types";
//Routing
import PrivateRoute from './routing/PrivateRoute'
//components
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Shop from "./components/WaiterDashboard/Shop";
const App = () => {
  useEffect(() => {
    // check for token in LS
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    store.dispatch(loadUser());

    // log user out from all tabs if they log out in one tab
    window.addEventListener("storage", () => {
      if (!localStorage.token) store.dispatch({ type: LOGOUT });
    });
  }, []);
  return (
    <Provider store={store}>
      <Fragment>
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <PrivateRoute exact path="/:shopID/main" component={Shop} />
          </Switch>
        </Router>
      </Fragment>
    </Provider>
  );
};

export default App;
