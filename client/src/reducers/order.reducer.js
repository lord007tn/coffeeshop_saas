import {
  GET_SHOP_ORDERS,
  GET_SHOP_ORDER,
  SEARCH_SHOP_ORDERS,
  CREATE_SHOP_ORDER,
  DELETE_SHOP_ORDER,
  UPDATE_SHOP_ORDER,
  SHOP_ORDER_ERROR
} from "../actions/types";

const initialState = {
  order: null,
  orders: [],
  paginationData: {},
  loading: true,
  error: {},
};
export default function (state = initialState, action) {
  const { type, payload, paginationData } = action;
  switch (type) {
    case GET_SHOP_ORDERS:
      return {
        ...state,
        orders: payload,
        paginationData: paginationData,
        loading: false,
      };
    case GET_SHOP_ORDER:
      return {
        ...state,
        order: payload,
        loading: false,
      };
    case CREATE_SHOP_ORDER:
      return {
        ...state,
        order: payload,
        loading: false,
      };
    case UPDATE_SHOP_ORDER:
      return {
        ...state,
        order: payload,
        loading: false,
      };
    case DELETE_SHOP_ORDER:
      return {
        ...state,
        orders: state.orders.filter((order) => order._id !== payload),
        loading: false,
      };
    case SEARCH_SHOP_ORDERS:
      return {
        ...state,
        orders: payload,
        paginationData: {},
        loading: false,
      };
    case SHOP_ORDER_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    default:
      return state;
  }
}
