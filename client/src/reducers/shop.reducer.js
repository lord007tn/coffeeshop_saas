import {
  GET_SHOP,
  GET_SHOPS_BY_OWNER,
  CREATE_SHOP,
  DELETE_SHOP,
  UPDATE_SHOP,
  SHOP_ERROR,
} from "../actions/types";

const initialState = {
  shop: null,
  shops: [],
  loading: true,
  error: {},
};
export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_SHOP:
      return {
        ...state,
        shop: payload,
        loading: false,
      };
    case CREATE_SHOP:
      return {
        ...state,
        shop: payload,
        loading: false,
      };
    case UPDATE_SHOP:
      return {
        ...state,
        shop: payload,
        loading: false,
      };
    case DELETE_SHOP:
      return {
        ...state,
        shops: state.shops.filter((shop) => shop._id !== payload),
        loading: false,
      };
    case GET_SHOPS_BY_OWNER:
      return {
        ...state,
        shops: payload,
        loading: false,
      };
    case SHOP_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    default:
      return state;
  }
}
