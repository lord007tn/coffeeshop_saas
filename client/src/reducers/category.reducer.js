import {
  GET_SHOP_CATEGORIES,
  GET_SHOP_CATEGORY,
  SEARCH_SHOP_CATEGORIES,
  CREATE_SHOP_CATEGORY,
  DELETE_SHOP_CATEGORY,
  UPDATE_SHOP_CATEGORY,
  SHOP_CATEGORY_ERROR
} from "../actions/types";

const initialState = {
  category: null,
  categories: [],
  paginationData: {},
  loading: true,
  error: {},
};
export default function (state = initialState, action) {
  const { type, payload, paginationData } = action;
  switch (type) {
    case GET_SHOP_CATEGORIES:
      return {
        ...state,
        categories: payload,
        paginationData: paginationData,
        loading: false,
        error: {},
      };
    case GET_SHOP_CATEGORY:
      return {
        ...state,
        category: payload,
        loading: false,
        error: {},
      };
    case CREATE_SHOP_CATEGORY:
      return {
        ...state,
        category: payload,
        loading: false,
        error: {},
      };
    case UPDATE_SHOP_CATEGORY:
      return {
        ...state,
        category: payload,
        loading: false,
        error: {},
      };
    case DELETE_SHOP_CATEGORY:
      return {
        ...state,
        categories: state.categories.filter(
          (category) => category._id !== payload
        ),
        loading: false,
        error: {},
      };
    case SEARCH_SHOP_CATEGORIES:
      return {
        ...state,
        categories: payload,
        paginationData: {},
        loading: false,
        error: {},
      };
    case SHOP_CATEGORY_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      }
    default:
      return state;
  }
}
