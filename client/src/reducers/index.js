import {combineReducers} from 'redux';
import auth from './auth.reducer'
import shop from "./shop.reducer";
import item from "./item.reducer";
import order from "./order.reducer";
import table from "./table.reducer";
import category from "./category.reducer";
export default combineReducers({
    auth,
    shop,
    item,
    order,
    table,
    category
})