import {
  GET_SHOP_TABLES,
  GET_SHOP_TABLE,
  SEARCH_SHOP_TABLES,
  CREATE_SHOP_TABLE,
  DELETE_SHOP_TABLE,
  UPDATE_SHOP_TABLE,
  SHOP_TABLE_ERROR
} from "../actions/types";

const initialState = {
  table: null,
  tables: [],
  paginationData: {},
  loading: true,
  error: {},
};
export default function (state = initialState, action) {
  const { type, payload, paginationData } = action;
  switch (type) {
    case GET_SHOP_TABLES:
      return {
        ...state,
        tables: payload,
        paginationData: paginationData,
        loading: false,
      };
    case GET_SHOP_TABLE:
      return {
        ...state,
        table: payload,
        loading: false,
      };
    case CREATE_SHOP_TABLE:
      return {
        ...state,
        table: payload,
        loading: false,
      };
    case UPDATE_SHOP_TABLE:
      return {
        ...state,
        table: payload,
        loading: false,
      };
    case DELETE_SHOP_TABLE:
      return {
        ...state,
        tables: state.tables.filter((table) => table._id !== payload),
        loading: false,
      };
    case SEARCH_SHOP_TABLES:
      return {
        ...state,
        tables: payload,
        paginationData: {},
        loading: false,
      };
    case SHOP_TABLE_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    default:
      return state;
  }
}
