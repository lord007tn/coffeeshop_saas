import {
  GET_SHOP_ITEMS,
  GET_SHOP_ITEM,
  GET_SHOP_ITEMS_BY_CATEGORY,
  SEARCH_SHOP_ITEMS,
  CREATE_SHOP_ITEM,
  DELETE_SHOP_ITEM,
  UPDATE_SHOP_ITEM,
  SHOP_ITEM_ERROR
} from "../actions/types";

const initialState = {
  item: null,
  items: [],
  paginationData: {},
  loading: true,
  error: {},
};
export default function (state = initialState, action) {
  const { type, payload, paginationData } = action;
  switch (type) {
    case GET_SHOP_ITEMS:
      return {
        ...state,
        items: payload,
        paginationData: paginationData,
        loading: false,
      };
    case GET_SHOP_ITEM:
      return {
        ...state,
        item: payload,
        loading: false,
      };
    case CREATE_SHOP_ITEM:
      return {
        ...state,
        item: payload,
        loading: false,
      };
    case UPDATE_SHOP_ITEM:
      return {
        ...state,
        item: payload,
        loading: false,
      };
    case DELETE_SHOP_ITEM:
      return {
        ...state,
        items: state.items.filter((item) => item._id !== payload),
        loading: false,
      };
    case SEARCH_SHOP_ITEMS:
      return {
        ...state,
        items: payload,
        paginationData: {},
        loading: false,
      };
    case GET_SHOP_ITEMS_BY_CATEGORY:
      return {
        ...state,
        items: payload,
        paginationData: {},
        loading: false,
      };
    case SHOP_ITEM_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    default:
      return state;
  }
}
