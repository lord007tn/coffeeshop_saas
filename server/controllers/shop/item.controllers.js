const Item = require( "../../models/shop/item.models" );
const { createItemDataValidation, itemDataValidation } = require( "../../validations/shop/item.validations" );
const getShopItems = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1;
	const shop = req.shop;
	try {
		const count = await Item.countDocuments( { shop: shop._id } );
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true : false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const items = await Item.find( { shop: shop._id } ).skip( ( page - 1 ) * pagination ).limit( pagination ).lean();
		return res.status( 200 ).json( { items: items, paginationData:{ count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getShopItemsByCategory = async ( req, res ) => {

	const shop = req.shop;
	const category = req.category;
	try {
		const items = await Item.find( { shop: shop._id, category: category._id } );
		return res.status( 200 ).json( { items: items } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getShopItem = async ( req, res ) => {

	const item = req.item;
	try {
		return res.status( 200 ).json( { item: item } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const createShopItem = async ( req, res ) => {

	const shop = req.shop;
	const { error } = await createItemDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	const item = new Item( {
		itemName: req.body.itemName,
		itemImage: req.body.itemImage,
		shop: req.shop._id,
		price: req.body.price,
		category: req.body.category
	} );
	try {
		const savedItem = await item.save();
		await shop.addItem( savedItem._id );
		return res.status( 201 ).json( { savedItem: savedItem } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateShopItem = async( req , res )=> {

	const item = req.item;
	const { error } = await itemDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updatedItem = await Item.findByIdAndUpdate( item._id, updateData, { new :true } );
		return res.status( 200 ).json( { updatedItem: updatedItem } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const deleteShopItem = async ( req, res ) => {
	const item = req.item;
	const shop = req.shop;
	try {
		const deletedItem = await Item.findByIdAndDelete( item._id ).lean();
		await shop.removeItem( deletedItem._id );
		return res.status( 200 ).json( { deletedItem: deletedItem } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const searchShopItems = async ( req, res ) => {
	const shop = req.shop;
	const value = req.query.itemName;
	const limit = req.query.limit ? parseInt( req.query.limit ) : 10;
	try {
		const items = await Item.find( { shop: shop._id ,itemName: { $regex: value, $options: "i" } } ).limit( limit ).lean();
		return res.status( 200 ).json( { items: items } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.searchShopItems = searchShopItems;
module.exports.createShopItem = createShopItem;
module.exports.getShopItems = getShopItems;
module.exports.getShopItem = getShopItem;
module.exports.getShopItemsByCategory = getShopItemsByCategory;
module.exports.updateShopItem = updateShopItem;
module.exports.deleteShopItem = deleteShopItem;
