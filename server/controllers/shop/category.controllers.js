const Category = require( "../../models/shop/category.models" );
const { createCategoryDataValidation, categoryDataValidation } = require( "../../validations/shop/category.validations" );
const getShopCategories = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1;
	const shop = req.shop;
	try {
		const count = await Category.countDocuments( { shop: shop._id } );
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true : false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const categories = await Category.find( { shop: shop._id } ).skip( ( page - 1 ) * pagination ).limit( pagination ).lean();
		return res.status( 200 ).json( { categories: categories, paginationData:{ count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getShopCategory = async ( req, res ) => {

	const category = req.category;
	try {
		return res.status( 200 ).json( { category: category } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const createShopCategory = async ( req, res ) => {

	const shop = req.shop;
	const { error } = await createCategoryDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	const category = new Category( {
		categoryName: req.body.categoryName,
		categoryImage: req.body.categoryImage,
		shop : req.shop._id
	} );
	try {
		const savedCategory = await category.save();
		await shop.addCategory( savedCategory._id );
		return res.status( 201 ).json( { savedCategory: savedCategory } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateShopCategory = async( req , res )=> {

	const category = req.category;
	const { error } = await categoryDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updatedCategory = await Category.findByIdAndUpdate( category._id, updateData, { new :true } );
		return res.status( 200 ).json( { updatedCategory: updatedCategory } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const deleteShopCategory = async ( req, res ) => {
	const category = req.category;
	const shop = req.shop;
	try {
		const deletedCategory = await Category.findByIdAndDelete( category._id ).lean();
		await shop.removeCategory( deletedCategory._id );
		return res.status( 200 ).json( { deletedCategory: deletedCategory } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const searchShopCategories = async ( req, res ) => {
	const shop = req.shop;
	const value = req.query.categoryName;
	const limit = req.query.limit ? parseInt( req.query.limit ) : 10;
	try {
		const categories = await Category.find( { shop: shop._id ,categoryName: { $regex: value, $options: "i" } } ).limit( limit ).lean();
		return res.status( 200 ).json( { categories: categories } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.searchShopCategories = searchShopCategories;
module.exports.createShopCategory = createShopCategory;
module.exports.getShopCategories = getShopCategories;
module.exports.getShopCategory = getShopCategory;
module.exports.updateShopCategory = updateShopCategory;
module.exports.deleteShopCategory = deleteShopCategory;
