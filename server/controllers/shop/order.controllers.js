const Order = require( "../../models/shop/order.models" );
const { createOrderDataValidation, orderDataValidation } = require( "../../validations/shop/order.validations" );
const getShopOrders = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1;
	const shop = req.shop;
	try {
		const count = await Order.countDocuments( { shop: shop._id } );
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true : false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const orders = await Order.find( { shop: shop._id } ).skip( ( page - 1 ) * pagination ).limit( pagination ).lean();
		return res.status( 200 ).json( { orders: orders, paginationData:{ count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getShopOrder = async ( req, res ) => {

	const order = req.order;
	try {
		return res.status( 200 ).json( { order: order } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const createShopOrder = async ( req, res ) => {

	const { error } = await createOrderDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	const order = new Order( {
		shop: req.shop._id,
		waiter: req.verifiedUser._id,
		items: req.body.items,
		total: req.body.total,
		table: req.body.table,
		note: req.body.note
	} );
	try {
		const savedOrder = await order.save();
		return res.status( 201 ).json( { savedOrder: savedOrder } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateShopOrder = async( req , res )=> {

	const order = req.order;
	const { error } = await orderDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updatedOrder = await Order.findByIdAndUpdate( order._id, updateData, { new :true } );
		return res.status( 200 ).json( { updatedOrder: updatedOrder } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const deleteShopOrder = async ( req, res ) => {
	const order = req.order;
	try {
		const deletedOrder = await Order.findByIdAndDelete( order._id ).lean();
		return res.status( 200 ).json( { deletedOrder: deletedOrder } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const searchShopOrders = async ( req, res ) => {
	const shop = req.shop;
	const value = req.query.orderID;
	const limit = req.query.limit ? parseInt( req.query.limit ) : 10;
	try {
		const orders = await Order.find( { shop: shop._id ,_id: value } ).limit( limit ).lean();
		return res.status( 200 ).json( { orders: orders } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.searchShopOrders = searchShopOrders;
module.exports.createShopOrder = createShopOrder;
module.exports.getShopOrders = getShopOrders;
module.exports.getShopOrder = getShopOrder;
module.exports.updateShopOrder = updateShopOrder;
module.exports.deleteShopOrder = deleteShopOrder;
