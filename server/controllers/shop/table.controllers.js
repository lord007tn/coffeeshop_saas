const Table = require( "../../models/shop/table.models" );
const { createTableDataValidation, tableDataValidation } = require( "../../validations/shop/table.validations" );
const getShopTables = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1;
	const shop = req.shop;
	try {
		const count = await Table.countDocuments( { shop: shop._id } );
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true : false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const tables = await Table.find( { shop: shop._id } ).skip( ( page - 1 ) * pagination ).limit( pagination ).lean();
		return res.status( 200 ).json( { tables: tables, paginationData:{ count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getShopTable = async ( req, res ) => {

	const table = req.table;
	try {
		return res.status( 200 ).json( { table: table } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const createShopTable = async ( req, res ) => {
	const shop = req.shop;
	const { error } = await createTableDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	const table = new Table( {
		tableName: req.body.tableName,
		shop : req.shop._id
	} );
	try {
		const savedTable = await table.save();
		await shop.addTable( savedTable._id );
		return res.status( 201 ).json( { savedTable: savedTable } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateShopTable = async( req , res )=> {

	const table = req.table;
	const { error } = await tableDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updatedTable = await Table.findByIdAndUpdate( table._id, updateData, { new :true } );
		return res.status( 200 ).json( { updatedTable: updatedTable } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const deleteShopTable = async ( req, res ) => {
	const table = req.table;
	const shop = req.shop;
	try {
		const deletedTable = await Table.findByIdAndDelete( table._id ).lean();
		await shop.removeTable( deletedTable._id );
		return res.status( 200 ).json( { deletedTable: deletedTable } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const searchShopTables = async ( req, res ) => {
	const shop = req.shop;
	const value = req.query.tableName;
	const limit = req.query.limit ? parseInt( req.query.limit ) : 10;
	try {
		const tables = await Table.find( { shop: shop._id ,tableName: { $regex: value, $options: "i" } } ).limit( limit ).lean();
		return res.status( 200 ).json( { tables: tables } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.searchShopTables = searchShopTables;
module.exports.createShopTable = createShopTable;
module.exports.getShopTables = getShopTables;
module.exports.getShopTable = getShopTable;
module.exports.updateShopTable = updateShopTable;
module.exports.deleteShopTable = deleteShopTable;
