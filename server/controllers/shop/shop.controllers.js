const Shop = require( "../../models/shop/shop.models" );
const { createShopDataValidation, shopDataValidation } = require( "../../validations/shop/shop.validations" );
const getShop = async ( req, res ) => {
	const shop = req.shop;

	try {
		return res.status( 200 ).json( { shop: shop } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const createShop = async ( req, res ) => {
	const { error } = await createShopDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	const shop = new Shop( {
		shopName: req.body.shopName,
		owners: req.body.owners
	} );
	try {
		const savedShop = await shop.save();
		return res.status( 201 ).json( { savedShop: savedShop } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const updateShop = async( req , res )=> {

	const shop = req.shop;
	const { error } = await shopDataValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updatedShop = await Shop.findByIdAndUpdate( shop._id, updateData, { new: true } );
		return res.status( 200 ).json( { updatedShop: updatedShop } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const deleteShop = async ( req, res ) => {
	const shop = req.shop;
	try {
		const deletedShop = await Shop.findByIdAndDelete( shop._id ).lean();
		return res.status( 200 ).json( { deletedShop: deletedShop } );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.getShop = getShop;
module.exports.createShop = createShop;
module.exports.updateShop = updateShop;
module.exports.deleteShop = deleteShop;
