const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;
const TableSchema = new Schema( {
	tableName: { type: String, maxlength: 256, lowercase: true, required: [ true, "Table name can't be blank" ] },
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop" }
}, { timestamps: true } );
module.exports = mongoose.model( "Table", TableSchema ) || mongoose.models.Table;
