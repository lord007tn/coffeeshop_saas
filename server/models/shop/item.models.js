const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;
const ItemSchema = new Schema( {
	itemName: { type: String, maxlength: 256, lowercase: true, required: [ true, "Item name can't be blank" ] },
	category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop" },
	price: { type: Number, required: true },
	itemImage: { type: String }
}, { timestamps: true } );
module.exports = mongoose.model( "Item", ItemSchema ) || mongoose.models.Item;
