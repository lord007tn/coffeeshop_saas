const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const slug = require( "slug" );
const Schema = mongoose.Schema;
const ShopSchema = new Schema( {
	shopName: { type: String, maxlength: 512, unique: true, lowercase: true, required: [ true, "Shop name can't be blank" ] },
	slug: { type: String, required: true },
	owners: [ { type: mongoose.Schema.Types.ObjectId, ref: "User" } ],
	categories: [ { type: mongoose.Schema.Types.ObjectId, ref: "Category" } ],
	items: [ { type: mongoose.Schema.Types.ObjectId, ref: "Item" } ],
	waiters: [ { type: mongoose.Schema.Types.ObjectId, ref: "User" } ],
	tables: [ { type: mongoose.Schema.Types.ObjectId, ref: "Table" } ]
}, { timestamps: true } );
ShopSchema.plugin( uniqueValidator, { message: "Shop name is already taken." } );

ShopSchema.pre( "validate", function ( next ) {
	if ( !this.slug ) {
		this.slug = slug( this.shopName );
	}
	next();
} );

ShopSchema.methods.addOwner = function( id ) {
	if ( this.owners.indexOf( id ) === -1 ) {
		this.owners.push( id );
	}
	this.save();
};
ShopSchema.methods.removeOwner = function( id ) {
	if ( this.owners.indexOf( id ) !== -1 ) {
		this.owners = this.owners.filter( ownerID => ownerID !== id );
	}
	this.save();
};
ShopSchema.methods.addCategory = function( id ) {
	if ( this.categories.indexOf( id ) === -1 ) {
		this.categories.push( id );
	}
	this.save();
};
ShopSchema.methods.removeCategory = function( id ) {
	if ( this.categories.indexOf( id ) !== -1 ) {
		this.categories = this.categories.filter( categoryID => categoryID !== id );
	}
	this.save();
};
ShopSchema.methods.addItem = function( id ) {
	if ( this.items.indexOf( id ) === -1 ) {
		this.items.push( id );
	}
	this.save();
};
ShopSchema.methods.removeItem = function( id ) {
	if ( this.items.indexOf( id ) !== -1 ) {
		this.items = this.items.filter( itemID => itemID !== id );
	}
	this.save();
};
ShopSchema.methods.addWaiter = function( id ) {
	if ( this.waiters.indexOf( id ) === -1 ) {
		this.waiters.push( id );
	}
	this.save();
};
ShopSchema.methods.removeWaiter = function( id ) {
	if ( this.waiters.indexOf( id ) !== -1 ) {
		this.waiters = this.waiters.filter( waiterID => waiterID !== id );
	}
	this.save();
};
ShopSchema.methods.addTable = function( id ) {
	if ( this.tables.indexOf( id ) === -1 ) {
		this.tables.push( id );
	}
	this.save();
};
ShopSchema.methods.removeTable = function( id ) {
	if ( this.tables.indexOf( id ) !== -1 ) {
		this.tables = this.tables.filter( tableID => tableID !== id );
	}
	this.save();
};
module.exports = mongoose.model( "Shop", ShopSchema ) || mongoose.models.Shop;
