const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;
const CategorySchema = new Schema( {
	categoryName: { type: String, maxlength: 256, lowercase: true, required: [ true, "Category name can't be blank" ] },
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop" },
	categoryImage: { type: String }
}, { timestamps: true } );
module.exports = mongoose.model( "Category", CategorySchema ) || mongoose.models.Category;
