const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;
const OrderSchema = new Schema( {
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop" },
	waiter: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	items: [ {
		item: { type: mongoose.Schema.Types.ObjectId, ref: "Item" },
		quantity: { type: Number, default: 1 },
		price: { type: Number }
	} ],
	total: { type: Number },
	isDelivered: { type: Boolean, default: false },
	isPayed: { type: Boolean, default: false },
	table: { type: mongoose.Schema.Types.ObjectId, ref: "Table" },
	note: { type: String, maxlength:512 }
}, { timestamps: true } );
module.exports = mongoose.model( "Order", OrderSchema ) || mongoose.models.Order;
