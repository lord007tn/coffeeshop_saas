const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const UserSchema = new Schema( {
	firstName: { type: String, maxlength: 64 },
	lastName: { type:String, maxlength: 64 },
	email: { type: String, required:[ true, "Email can't be blank" ], index:true, lowercase: true, unique: true },
	phoneNumber: { type: Number, required:[ true, "Phone number can't be blank" ], index:true, unique: true },
	password: { type: String, required: [ true, "Password can't be blank" ], maxlength: 1024 },
	isManager: { type: Boolean, default: true },
	isWaiter: { type: Boolean, default: true },
	isActive: { type: Boolean, default: false },
	shop: { type: mongoose.Schema.Types.ObjectId, ref: "Shop" }
}, { timestamps: true } );

UserSchema.plugin( uniqueValidator, { message: "is already taken." } );

UserSchema.methods.authToJSON = function(){
	return {
		_id: this._id,
		email: this.email,
		firstName: this.firstName,
		lastName: this.lastName,
		isManager: this.isManager,
		isWaiter: this.isWaiter,
		shop: this.shop
	};
};

UserSchema.methods.userProfile = function(){
	return {
		_id: this._id,
		email: this.email,
		phoneNumber: this.phoneNumber,
		firstName: this.firstName,
		lastName: this.lastName,
		isManager: this.isManager,
		isWaiter: this.isWaiter,
		shop: this.shop
	};
};
module.exports = mongoose.model( "User", UserSchema ) || mongoose.models.User;
