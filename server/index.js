const express = require( "express" );
const app = express();
const bodyParser = require( "body-parser" );
const mongoose = require( "mongoose" );
const dotenv = require( "dotenv" );
const morgan = require( "morgan" );
const helmet = require( "helmet" );
const cors = require( "cors" );
const compression = require( "compression" );
const errorhandler = require( "errorhandler" );
//Dotenv config
dotenv.config();
//Connect to database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect( process.env.MONGO_DB_LOCAL );
mongoose.connection.on( "connected", () => {
	console.log( "DB Connected" );
} );
mongoose.connection.on( "error", ( err ) => {
	console.log( "DB Connection failed with - ", err );
} );
//Import routes
const authRoutes = require( "./routes/auth.routes" );
const itemRoutes = require( "./routes/shop/item.routes" );
const shopRoutes = require( "./routes/shop/shop.routes" );
const categoryRoutes = require( "./routes/shop/category.routes" );
const tableRoutes = require( "./routes/shop/table.routes" );
const orderRoutes = require( "./routes/shop/order.routes" );
//Middleware
app.use( helmet() );
app.use( cors() );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended : false } ) );
app.use( morgan( "dev" ) );
app.use( compression() );
if ( process.env.NODE_ENV === "development" ) {
	// only use in development
	app.use( errorhandler() );
}
//Routes middleware
app.use( "/api", authRoutes );
app.use( "/api/shops", shopRoutes );
app.use( "/api/items", itemRoutes );
app.use( "/api/categories", categoryRoutes );
app.use( "/api/tables", tableRoutes );
// app.use( "/api/orders", orderRoutes );
//Server run
const port = 8000 || process.env.PORT;
app.listen( port, ()=>{
	console.log( "Server is up and running on port number " + port );
} );
