module.exports = function ( req, res, next ) {
	const user = req.verifiedUser;
	const shop = req.shop;
	if ( user.shop === shop._id.toString() ) {
		next();
	}
	else {
		return res.status( 403 ).json( { message: "Forbidden access, your are not the Owner" } );
	}
};
