module.exports = function ( req, res, next ){
	const user = req.verifiedUser;
	if ( user.isActive ) {
		next();
	}
	else {
		return res.status( 403 ).json( { message: "Forbidden access, you have to activate your account" } );
	}
};
