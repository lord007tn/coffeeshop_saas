module.exports = function ( req, res, next ){
	const user = req.verifiedUser;
	if ( user.isManager ) {
		next();
	}
	else {
		return res.status( 403 ).json( { message: "Forbidden access, only Manager" } );
	}
};
