const joi = require( "joi" );

module.exports.createItemDataValidation = data => {
	const schema = joi.object( {
		itemName: joi.string().max( 256 ).required(),
		category: joi.string().required(),
		price: joi.number().required(),
		itemImage: joi.any()
	} );
	return schema.validate( data );
};

module.exports.itemDataValidation = data => {
	const schema = joi.object( {
		itemName: joi.string().max( 256 ),
		category: joi.array().items( joi.string() ),
		price: joi.number(),
		itemImage: joi.any()
	} );
	return schema.validate( data );
};
