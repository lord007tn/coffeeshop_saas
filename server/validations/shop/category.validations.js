const joi = require( "joi" );

module.exports.createCategoryDataValidation = data => {
	const schema = joi.object( {
		categoryName: joi.string().max( 256 ).required(),
		categoryImage: joi.any()
	} );
	return schema.validate( data );
};

module.exports.categoryDataValidation = data => {
	const schema = joi.object( {
		categoryName: joi.string().max( 256 ),
		categoryImage: joi.any()
	} );
	return schema.validate( data );
};
