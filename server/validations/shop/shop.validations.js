const joi = require( "joi" );

module.exports.createShopDataValidation = data => {
	const schema = joi.object( {
		shopName: joi.string().max( 512 ).required(),
		owners: joi.array( joi.string() ).required()
	} );
	return schema.validate( data );
};

module.exports.shopDataValidation = data => {
	const schema = joi.object( {
		shopName: joi.string(),
		owners: joi.array( joi.string() ),
		categories: joi.array( joi.string() ),
		items: joi.array( joi.string() ),
		waiters: joi.array( joi.string() ),
		tables: joi.array( joi.string() )
	} );
	return schema.validate( data );
};
