const joi = require( "joi" );

module.exports.createTableDataValidation = data => {
	const schema = joi.object( {
		tableName: joi.string().max( 256 ).required()
	} );
	return schema.validate( data );
};

module.exports.tableDataValidation = data => {
	const schema = joi.object( {
		tableName: joi.string().max( 256 )
	} );
	return schema.validate( data );
};
