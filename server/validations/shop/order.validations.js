const joi = require( "joi" );

module.exports.createOrderDataValidation = data => {
	const schema = joi.object( {
		items: joi.array().items( joi.object(
			{ item: joi.string(),
				quantity: joi.number(),
				price: joi.number() } ) ).required(),
		total: joi.number().required(),
		table: joi.string().required(),
		note: joi.string().max( 512 ).required()
	} );
	return schema.validate( data );
};

module.exports.orderDataValidation = data => {
	const schema = joi.object( {
		items: joi.array().items( joi.object(
			{ item: joi.string(),
				quantity: joi.number(),
				price: joi.number() } ) ),
		total: joi.number(),
		table: joi.string(),
		note: joi.string().max( 512 )
	} );
	return schema.validate( data );
};
