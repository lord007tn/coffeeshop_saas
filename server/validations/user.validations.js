const joi = require( "joi" );

module.exports.userLoginValidation = data => {
	const schema = joi.object( {
		loginInfo: joi.string().min( 6 ).required(),
		password: joi.string().min( 6 ).required()
	} );
	return schema.validate( data );
};
module.exports.userRegisterValidation = data => {
	const schema = joi.object( {
		firstName: joi.string().max( 64 ).required(),
		lastName:  joi.string().max( 64 ).required(),
		email: joi.string().min( 6 ).email().required(),
		phoneNumber: joi.number().required(),
		password: joi.string().min( 8 ).required(),
		shopName: joi.string().max( 512 ).required()
	} );
	return schema.validate( data );
};

module.exports.userDataValidation = data => {
	const schema = joi.object( {
		firstName: joi.string().max( 64 ),
		lastName:  joi.string().max( 64 ),
		email: joi.string().min( 6 ).email(),
		phoneNumber: joi.number(),
		password: joi.string().min( 8 )
	} );
	return schema.validate( data );
};
