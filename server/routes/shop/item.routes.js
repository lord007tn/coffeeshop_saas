const router = require( "express" ).Router();
const Item = require( "../../models/shop/item.models" );
const Shop = require( "../../models/shop/shop.models" );
const Category = require( "../../models/shop/category.models" );
const verifyToken = require( "../../utils/verifyToken" );
const isActive = require( "../../utils/isActive" );
const isShopOwner = require( "../../utils/isShopOwner" );
const isManager = require( "../../utils/isManager" );
const itemControllers = require( "../../controllers/shop/item.controllers" );
router.param( "shop", async ( req, res, next, id ) => {
	try {
		const shop = await Shop.findById( id );
		if ( !shop ) {
			return res.status( 404 ).json( { message: "Shop Not found" } );
		}
		req.shop = shop;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.param( "item", async ( req, res, next, id ) => {
	try {
		const item = await Item.findOne( { _id: id, shop: req.shop._id } );
		if ( !item ) {
			return res.status( 404 ).json( { message: "Item Not found" } );
		}
		req.item = item;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.param( "category", async ( req, res, next, id ) => {
	try {
		const category = await Category.findOne( { _id: id, shop: req.shop._id } );
		if ( !category ) {
			return res.status( 404 ).json( { message: "Category Not found" } );
		}
		req.category = category;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );

router.get( "/:shop/", verifyToken, isActive, isShopOwner, itemControllers.getShopItems );
router.get( "/:shop/search", verifyToken, isActive, isShopOwner, itemControllers.searchShopItems );
router.get( "/:shop/:category/items", verifyToken, isActive, isShopOwner, itemControllers.getShopItemsByCategory );
router.get( "/:shop/:item",verifyToken, isActive, isShopOwner, itemControllers.getShopItem );
//Manager routes
router.post( "/:shop/create", verifyToken, isActive, isShopOwner, isManager, itemControllers.createShopItem );
router.put( "/:shop/:item/update", verifyToken, isActive, isShopOwner, isManager, itemControllers.updateShopItem );
router.delete( "/:shop/:item/delete",verifyToken, isActive, isShopOwner, isManager, itemControllers.deleteShopItem );
module.exports = router;
