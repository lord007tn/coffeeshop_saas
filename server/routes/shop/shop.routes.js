const router = require( "express" ).Router();
const Shop = require( "../../models/shop/shop.models" );
const verifyToken = require( "../../utils/verifyToken" );
const isActive = require( "../../utils/isActive" );
const isShopOwner = require( "../../utils/isShopOwner" );
const isManager = require( "../../utils/isManager" );
const shopControllers = require( "../../controllers/shop/shop.controllers" );
router.param( "shop", async ( req, res, next, id ) => {
	try {
		const shop = await Shop.findById( id );
		if ( !shop ) {
			return res.status( 404 ).json( { message: "Shop Not found" } );
		}
		req.shop = shop;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );

router.get( "/:shop", verifyToken, isActive, isShopOwner, shopControllers.getShop );
router.post( "/create", verifyToken, isActive, isShopOwner, isManager, shopControllers.createShop );
module.exports = router;
