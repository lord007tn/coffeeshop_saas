const router = require( "express" ).Router();
const Table = require( "../../models/shop/table.models" );
const Shop = require( "../../models/shop/shop.models" );
const verifyToken = require( "../../utils/verifyToken" );
const isActive = require( "../../utils/isActive" );
const isShopOwner = require( "../../utils/isShopOwner" );
const isManager = require( "../../utils/isManager" );
const tableControllers = require( "../../controllers/shop/table.controllers" );
router.param( "shop", async ( req, res, next, id ) => {
	try {
		const shop = await Shop.findById( id );
		if ( !shop ) {
			return res.status( 404 ).json( { message: "Shop Not found" } );
		}
		req.shop = shop;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.param( "table", async ( req, res, next, id ) => {
	try {
		const table = await Table.findOne( { _id: id, shop: req.shop._id } );
		if ( !table ) {
			return res.status( 404 ).json( { message: "Table Not found" } );
		}
		req.table = table;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );

router.get( "/:shop/", verifyToken, isActive, isShopOwner, tableControllers.getShopTables );
router.get( "/:shop/search", verifyToken, isActive, isShopOwner, tableControllers.searchShopTables );
router.get( "/:shop/:table",verifyToken, isActive, isShopOwner, tableControllers.getShopTable );
//Manager routes
router.post( "/:shop/create", verifyToken, isActive, isShopOwner, isManager, tableControllers.createShopTable );
router.put( "/:shop/:table/update", verifyToken, isActive, isShopOwner, isManager, tableControllers.updateShopTable );
router.delete( "/:shop/:table/delete",verifyToken, isActive, isShopOwner, isManager, tableControllers.deleteShopTable );
module.exports = router;
