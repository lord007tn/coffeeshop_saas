const router = require( "express" ).Router();
const Category = require( "../../models/shop/category.models" );
const Shop = require( "../../models/shop/shop.models" );
const verifyToken = require( "../../utils/verifyToken" );
const isActive = require( "../../utils/isActive" );
const isShopOwner = require( "../../utils/isShopOwner" );
const isManager = require( "../../utils/isManager" );
const categoryControllers = require( "../../controllers/shop/category.controllers" );
router.param( "shop", async ( req, res, next, id ) => {
	try {
		const shop = await Shop.findById( id );
		if ( !shop ) {
			return res.status( 404 ).json( { message: "Shop Not found" } );
		}
		req.shop = shop;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.param( "category", async ( req, res, next, id ) => {
	try {
		const category = await Category.findOne( { _id: id, shop: req.shop._id } );
		if ( !category ) {
			return res.status( 404 ).json( { message: "Category Not found" } );
		}
		req.category = category;
		next();
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );

router.get( "/:shop/", verifyToken, isActive, isShopOwner, categoryControllers.getShopCategories );
router.get( "/:shop/search", verifyToken, isActive, isShopOwner, categoryControllers.searchShopCategories );
router.get( "/:shop/:category",verifyToken, isActive, isShopOwner, categoryControllers.getShopCategory );
//Manager routes
router.post( "/:shop/create", verifyToken, isActive, isShopOwner, isManager, categoryControllers.createShopCategory );
router.put( "/:shop/:category/update", verifyToken, isActive, isShopOwner, isManager, categoryControllers.updateShopCategory );
router.delete( "/:shop/:category/delete",verifyToken, isActive, isShopOwner, isManager, categoryControllers.deleteShopCategory );
module.exports = router;
