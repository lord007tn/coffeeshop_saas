
const router = require( "express" ).Router();
const User = require( "../models/user.models" );
const Shop = require( "../models/shop/shop.models" );
const jwt = require( "jsonwebtoken" );
const bcrypt = require( "bcryptjs" );
const verifyToken = require( "../utils/verifyToken" );
const { userRegisterValidation, userLoginValidation } = require( "../validations/user.validations" );
const isEmail = require( "validator/lib/isEmail" );
router.get( "/authcheck", verifyToken, async ( req, res ) => {
	try {
		const user = await User.findById( req.verifiedUser._id );
		return res.status( 200 ).json( user.authToJSON() );
	}
	catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.post( "/register", async ( req, res )=> {
	//Validating the data we parse in body
	const { error } = await userRegisterValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	//Checking if the email is validation
	const existEmail = await User.findOne( { email: req.body.email } );
	if ( existEmail ) {
		return res.status( 400 ).json( "Email already exist" );
	}

	//Hashing the password
	const salt = await bcrypt.genSalt( 16 );
	const hashedPassword = await bcrypt.hash( req.body.password, salt );
	try {
	//Creating the user shop
		const shop = new Shop( {
			shopName: req.body.shopName
		} );
		await shop.save();
		//Creating new user
		const user = new User( {
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			phoneNumber: req.body.phoneNumber,
			email: req.body.email,
			password: hashedPassword,
			shop: shop._id
		} );
		await user.save();
		await shop.addOwner( user._id );
		return res.status( 200 ).json( { user: user.userProfile() } );
	}
	catch ( err ){
		return res.status( 500 ).json( err );
	}
} );
router.post( "/login", async ( req, res )=> {
	//Validating the data
	const { error } = await userLoginValidation( req.body );
	if ( error ) {
		return res.status( 422 ).json( { err_message: error } );
	}
	let user;
	if ( isEmail( req.body.loginInfo ) ) {
		//Checking if email is valid
		user = await User.findOne( { email: req.body.loginInfo } );
		if ( !user ) {
			return res.status( 401 ).json( { err_message: "Wrong Email or Password" } );
		}
	}
	else {
		//Checking if phone number is valid
		user = await User.findOne( { phoneNumber: req.body.loginInfo } );
		if ( !user ) {
			return res.status( 401 ).json( { err_message: "Wrong Phone number or Password" } );
		}
	}
	//Validate password
	const validPass = await bcrypt.compare( req.body.password, user.password );
	if ( !validPass ) {
		return res.status( 401 ).json( { err_message: "Wrong Email or Password" } );
	}
	//Generating Token
	const token = jwt.sign( { _id: user._id, isActive: user.isActive, isWaiter: user.isWaiter, isManager: user.isManager, shop: user.shop }, process.env.TOKEN_KEY, { expiresIn: "2 days" } );
	return res.header( "access_token", token ).status( 200 ).json( { token: token, user: user.authToJSON() } );
} );
module.exports = router;
